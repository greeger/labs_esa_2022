package com.example.lab3.model;

import com.example.lab3.model.Director;
import com.example.lab3.model.Movie;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DirectedBy implements Serializable {

    private Director director;

    private List<Movie> movies;
}
