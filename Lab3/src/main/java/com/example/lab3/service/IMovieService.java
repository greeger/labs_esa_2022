package com.example.lab3.service;

import com.example.lab3.model.Director;
import com.example.lab3.model.Movie;

import java.util.List;
import java.util.Optional;

public interface IMovieService {

    Movie add(Movie movie);
    Optional<Movie> get(long id);
    void delete(long id);
    List<Movie> findAll();
    List<Movie> getByDirector(Director director);
}