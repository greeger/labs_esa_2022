package com.example.lab3.service;

import com.example.lab3.model.Director;
import com.example.lab3.repository.DirectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DirectorService implements IDirectorService {

    @Autowired
    private DirectorRepository repository;

    @Override
    public Director add(Director director){
        return repository.save(director);
    }

    @Override
    public Optional<Director> get(long id){
        return repository.findById(id);
    }

    @Override
    public void delete(long id){
        repository.deleteById(id);
    }

    @Override
    public List<Director> findAll() {
        return (List<Director>) repository.findAll();
    }
}