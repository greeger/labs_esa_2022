insert into director(name, imdb)
values ('Quentin Tarantino', 'nm0000233'),
       ('Guy Ritchie', 'nm0005363'),
       ('Ridley Scott', 'nm0000631');

insert into movie(name, year, director_id, imdb)
values ('Pulp Fiction', 1994, 1, 'tt0110912'),
       ('The Hateful Eight', 2015, 1, 'tt3460252'),
       ('Kill Bill: The Whole Bloody Affair', 2011, 1, 'tt6019206'),
       ('Sherlock Holmes: A Game of Shadows', 2011, 2, 'tt1515091'),
       ('Wrath of Man', 2021, 2, 'tt11083552'),
       ('The Man from U.N.C.L.E.', 2015, 2, 'tt1638355'),
       ('Blade Runner', 1982, 3, 'tt0083658'),
       ('The Martian', 2015, 3, 'tt3659388'),
       ('Gladiator', 2000, 3, 'tt0172495');