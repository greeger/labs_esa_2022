<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">
    <xsl:output method="html" indent="yes" media-type="text/html" encoding="UTF-8" />
    <xsl:template match="/">
        <html>
            <head>
                <title><xsl:value-of select="DirectedBy/director/name"/></title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            </head>
            <body>
                <a href="/directors">All directors</a><br/>
                Directed by <xsl:value-of select="DirectedBy/director/name"/>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Year</th>
                        <th>IMDB</th>
                    </tr>

                    <xsl:variable name="director_id" select="DirectedBy/director/id"/>
                    <xsl:for-each select="DirectedBy/movies/movies">
                        <xsl:variable name="id" select="id"/>
                        <xsl:variable name="imdb" select="imdb"/>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="year"/></td>
                            <td><a href="https://www.imdb.com/title/{$imdb}"><xsl:value-of select="imdb"/></a></td>

                            <td>
                                <form action="/movies/{$director_id}/{$id}" method="post">
                                    <input type="submit" value="delete" />
                                </form>
                            </td>
                        </tr>
                    </xsl:for-each>
                    <tr>
                        <form action="/movies/{$director_id}" method="post">
                            <td><input type="text" name="name" /></td>
                            <td><input type="number" name="year" required="true" /></td>
                            <td><input type="text" name="imdb" /></td>
                            <td><input type="submit" value="add" /></td>
                        </form>
                    </tr>

                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>