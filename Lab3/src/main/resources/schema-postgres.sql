DROP TABLE IF EXISTS Movie;
DROP TABLE IF EXISTS Director;

CREATE TABLE Movie
(
    id           SERIAL PRIMARY KEY,
    name         TEXT,
    year         INT,
    director_id  INT,
    imdb         TEXT
);
CREATE TABLE Director
(
    id           SERIAL PRIMARY KEY,
    name         TEXT,
    imdb         TEXT
);

ALTER TABLE Movie
    ADD CONSTRAINT fk FOREIGN KEY (director_id) REFERENCES director (id) ON DELETE CASCADE;