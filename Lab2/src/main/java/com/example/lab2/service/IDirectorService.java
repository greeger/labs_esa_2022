package com.example.lab2.service;

import com.example.lab2.model.Director;

import java.util.List;
import java.util.Optional;

public interface IDirectorService {

    Director add(Director director);
    Optional<Director> get(long id);
    void delete(long id);
    List<Director> findAll();
}