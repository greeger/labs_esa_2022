package com.example.lab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity(name = "director")
@Table(name = "director")
public class Director implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "imdb")
    private String imdb;

    public Director(String name, String imdb) {
        this.name = name;
        this.imdb = imdb;
    }
}
