package com.example.lab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity(name = "movie")
@Table(name = "movie")
public class Movie implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "year")
    private int year;

    @ManyToOne
    private Director director;

    @Column(name = "imdb")
    private String imdb;

    public Movie(String movieName, int year, Director director, String imdb) {
        this.name = movieName;
        this.year = year;
        this.director = director;
        this.imdb = imdb;
    }
    public Movie(String movieName, int year, String imdb) {
        this.name = movieName;
        this.year = year;
        this.imdb = imdb;
    }
}
