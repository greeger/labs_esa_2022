package com.example.lab2.controller;

import com.example.lab2.model.Director;
import com.example.lab2.model.Movie;

import com.example.lab2.service.IDirectorService;
import com.example.lab2.service.IMovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MyController {

    @Autowired
    private IDirectorService directorService;
    @Autowired
    private IMovieService movieService;

    @GetMapping("/directors")
    public String findDirectors(Model model) {
        var directors = (List<Director>) directorService.findAll();

        model.addAttribute("directors", directors);

        return "directors";
    }

    @PostMapping("/directors")
    public String addDirector(Model model, @ModelAttribute Director director) {
        directorService.add(director);

        return "redirect:/directors";

    }

    @GetMapping("/directors/{id}")
    public String deleteDirector(Model model, @PathVariable String id) {
        directorService.delete(Integer.parseInt(id));

        return "redirect:/directors";
    }

    @GetMapping("/movies/{id}")
    public String findMovies(Model model, @PathVariable(value="id") String str_id) {
        int id = Integer.parseInt(str_id);
        var director = directorService.get(id).orElse(null);
        var movies = (List<Movie>) movieService.getByDirector(director);

        model.addAttribute("movies", movies);
        model.addAttribute("director", director);

        return "movies";
    }

    @PostMapping("/movies/{id}")
    public String addMovie(Model model, @PathVariable(value="id") String str_id, @ModelAttribute Movie movie) {
        int id = Integer.parseInt(str_id);
        var director = directorService.get(id).orElse(null);

        movieService.add(new Movie(movie.getName(), movie.getYear(), director, movie.getImdb()));

        var movies = (List<Movie>) movieService.getByDirector(director);

        model.addAttribute("movies", movies);
        model.addAttribute("director", director);

        return "movies";

    }

    @GetMapping("/movies/{director_id}/{movie_id}")
    public String deleteMovie(Model model, @PathVariable String director_id, @PathVariable String movie_id) {
        movieService.delete(Integer.parseInt(movie_id));

        return "redirect:/movies/"+director_id;
    }
}