package com.example.lab2.repository;

import com.example.lab2.model.Director;
import com.example.lab2.model.Movie;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {
    @Query("SELECT m FROM movie m where director=:director")
    Iterable<Movie> getByDirector(@Param("director") Director director);
}