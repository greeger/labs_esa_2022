package com.example.lab4.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity(name = "conditionEmail")
@Table(name = "condition_Email")
public class ConditionEmail implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "condition")
    private String condition;

    @Column(name = "email")
    private String email;

    public ConditionEmail(String condition, String email) {
        this.condition = condition;
        this.email = email;
    }
}
