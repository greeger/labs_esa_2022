package com.example.lab4.model;

import com.example.lab4.model.Director;
import com.example.lab4.model.Movie;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DirectedBy implements Serializable {

    private Director director;

    private List<Movie> movies;
}
