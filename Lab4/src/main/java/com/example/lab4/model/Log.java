package com.example.lab4.model;

import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity(name = "log")
@Table(name = "log")
public class Log implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "type")
    private String type;

    @Column(name = "table_name")
    private String table_name;

    @Column(name = "raw_id")
    private int raw_id;

    @Column(name = "info")
    private String info;

    public Log(String type, String table_name, int raw_id, String info) {
        this.type = type;
        this.table_name = table_name;
        this.raw_id = raw_id;
        this.info = info;
    }
}
