package com.example.lab4.controller;

import com.example.lab4.jms.Sender;
import com.example.lab4.model.Director;
import com.example.lab4.model.Log;
import com.example.lab4.model.Movie;
import com.example.lab4.model.DirectedBy;

import com.example.lab4.service.IDirectorService;
import com.example.lab4.service.IMovieService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.util.List;

@RestController
@RequestMapping(produces = {"application/xml"})
public class MyController {

    private final IDirectorService directorService;
    private final IMovieService movieService;
    private final Sender sender;
    @Autowired
    public MyController(IDirectorService directorService, IMovieService movieService, Sender sender) {
        this.directorService = directorService;
        this.movieService = movieService;
        this.sender = sender;
    }

    @GetMapping(value = "/directors", produces = MediaType.APPLICATION_XML_VALUE)
    public ModelAndView findDirectors() throws JsonProcessingException {
        final List<Director> directors = (List<Director>) directorService.findAll();

        ModelAndView modelAndView = new ModelAndView("directors");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(directors)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @PostMapping("/directors")
    public ModelAndView addDirector(@ModelAttribute Director director) throws JsonProcessingException {

        directorService.add(new Director(director.getName(), director.getImdb()));

        sender.sendMessage(new Log("add", "directors", director.getId(),
                "name="+director.getName()+"&imdb="+director.getImdb()));

        final List<Director> directors = (List<Director>) directorService.findAll();

        ModelAndView modelAndView = new ModelAndView("directors");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(directors)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @PostMapping("/directors/{id}")
    public ModelAndView deleteDirector(@PathVariable String id) throws JsonProcessingException {
        Director director = directorService.get(Integer.parseInt(id)).orElse(null);
        sender.sendMessage(new Log("delete", "directors", director.getId(),
                "name="+director.getName()+"&imdb="+director.getImdb()));
        directorService.delete(Integer.parseInt(id));

        final List<Director> directors = (List<Director>) directorService.findAll();

        ModelAndView modelAndView = new ModelAndView("directors");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(directors)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @GetMapping("/movies/{id}")
    public ModelAndView findMovies(@PathVariable String id) throws JsonProcessingException {
        var director = directorService.get(Integer.parseInt(id)).orElse(null);
        var movies = (List<Movie>) movieService.getByDirector(director);

        final DirectedBy directedBy = new DirectedBy(director, movies);

        ModelAndView modelAndView = new ModelAndView("movies");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(directedBy)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @PostMapping("/movies/{id}")
    public ModelAndView addMovie(@PathVariable String id, @ModelAttribute Movie movie) throws JsonProcessingException {
        var director = directorService.get(Integer.parseInt(id)).orElse(null);

        sender.sendMessage(new Log("add", "movies", movie.getId(),
                "name="+movie.getName()+"&year="+movie.getYear()+"&director_id="+id+"&imdb="+movie.getImdb()));

        movieService.add(new Movie(movie.getName(), movie.getYear(), director, movie.getImdb()));

        var movies = (List<Movie>) movieService.getByDirector(director);

        final DirectedBy directedBy = new DirectedBy(director, movies);

        ModelAndView modelAndView = new ModelAndView("movies");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(directedBy)));
        modelAndView.addObject(source);
        return modelAndView;
    }

    @PostMapping("/movies/{director_id}/{id}")
    public ModelAndView deleteMovie(@PathVariable String director_id, @PathVariable String id) throws JsonProcessingException {
        var director = directorService.get(Integer.parseInt(director_id)).orElse(null);

        Movie movie = movieService.get(Integer.parseInt(id)).orElse(null);
        sender.sendMessage(new Log("delete", "movies", movie.getId(),
                "name="+movie.getName()+"&year="+movie.getYear()+"&director_id="+director_id+"&imdb="+movie.getImdb()));

        movieService.delete(Integer.parseInt(id));

        var movies = (List<Movie>) movieService.getByDirector(director);

        final DirectedBy directedBy = new DirectedBy(director, movies);

        ModelAndView modelAndView = new ModelAndView("movies");
        Source source = new StreamSource(new ByteArrayInputStream(new XmlMapper().writeValueAsBytes(directedBy)));
        modelAndView.addObject(source);
        return modelAndView;
    }
}