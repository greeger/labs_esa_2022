package com.example.lab4.repository;

import com.example.lab4.model.Director;
import com.example.lab4.model.Movie;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Integer> {
    @Query("SELECT m FROM movie m where director=:director")
    Iterable<Movie> getByDirector(@Param("director") Director director);
}