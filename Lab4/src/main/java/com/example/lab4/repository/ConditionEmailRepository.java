package com.example.lab4.repository;

import com.example.lab4.model.ConditionEmail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConditionEmailRepository extends CrudRepository<ConditionEmail, Integer> {

}