package com.example.lab4.service;

import com.example.lab4.model.ConditionEmail;
import com.example.lab4.repository.ConditionEmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConditionEmailService implements IConditionEmailService {

    @Autowired
    private ConditionEmailRepository repository;

    @Override
    public ConditionEmail add(ConditionEmail conditionEmail){
        return repository.save(conditionEmail);
    }

    @Override
    public Optional<ConditionEmail> get(int id){
        return repository.findById(id);
    }

    @Override
    public void delete(int id){
        repository.deleteById(id);
    }

    @Override
    public List<ConditionEmail> findAll() {
        return (List<ConditionEmail>) repository.findAll();
    }
}