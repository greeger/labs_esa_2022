package com.example.lab4.service;

import com.example.lab4.model.Log;

import java.util.List;
import java.util.Optional;

public interface ILogService {

    Log add(Log log);
    Optional<Log> get(int id);
    void delete(int id);
    List<Log> findAll();
}