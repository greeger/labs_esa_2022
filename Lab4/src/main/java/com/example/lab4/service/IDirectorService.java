package com.example.lab4.service;

import com.example.lab4.model.Director;

import java.util.List;
import java.util.Optional;

public interface IDirectorService {

    Director add(Director director);
    Optional<Director> get(int id);
    void delete(int id);
    List<Director> findAll();
}