package com.example.lab4.service;

import com.example.lab4.model.Director;
import com.example.lab4.model.Movie;
import com.example.lab4.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService implements IMovieService {

    @Autowired
    private MovieRepository repository;

    @Override
    public Movie add(Movie movie){
        return repository.save(movie);
    }

    @Override
    public Optional<Movie> get(int id){
        return repository.findById(id);
    }

    @Override
    public void delete(int id){
        repository.deleteById(id);
    }

    @Override
    public List<Movie> findAll() {
        return (List<Movie>) repository.findAll();
    }

    @Override
    public List<Movie> getByDirector(Director director) {
        return (List<Movie>) repository.getByDirector(director);
    }
}