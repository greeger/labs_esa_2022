package com.example.lab4.service;

import com.example.lab4.model.Director;
import com.example.lab4.model.Movie;

import java.util.List;
import java.util.Optional;

public interface IMovieService {

    Movie add(Movie movie);
    Optional<Movie> get(int id);
    void delete(int id);
    List<Movie> findAll();
    List<Movie> getByDirector(Director director);
}