package com.example.lab4.service;

import com.example.lab4.model.Log;
import com.example.lab4.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LogService implements ILogService {

    @Autowired
    private LogRepository repository;

    @Override
    public Log add(Log log){
        return repository.save(log);
    }

    @Override
    public Optional<Log> get(int id){
        return repository.findById(id);
    }

    @Override
    public void delete(int id){
        repository.deleteById(id);
    }

    @Override
    public List<Log> findAll() {
        return (List<Log>) repository.findAll();
    }
}