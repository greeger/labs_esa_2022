package com.example.lab4.service;

import com.example.lab4.model.Director;
import com.example.lab4.repository.DirectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DirectorService implements IDirectorService {

    @Autowired
    private DirectorRepository repository;

    @Override
    public Director add(Director director){
        return repository.save(director);
    }

    @Override
    public Optional<Director> get(int id){
        return repository.findById(id);
    }

    @Override
    public void delete(int id){
        repository.deleteById(id);
    }

    @Override
    public List<Director> findAll() {
        return (List<Director>) repository.findAll();
    }
}