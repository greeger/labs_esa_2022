package com.example.lab4.service;

import com.example.lab4.model.ConditionEmail;

import java.util.List;
import java.util.Optional;

public interface IConditionEmailService {

    ConditionEmail add(ConditionEmail conditionEmail);
    Optional<ConditionEmail> get(int id);
    void delete(int id);
    List<ConditionEmail> findAll();
}