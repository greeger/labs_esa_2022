package com.example.lab4.utils;

import com.example.lab4.model.Log;

public class ConditionChecker {
    public static boolean conditionTriggered(Log log, String condition){
        if(condition.contains("|")){
            String[] conditions = condition.split("\\|");
            for(String newCondition: conditions)
                if (conditionTriggered(log, newCondition))
                    return true;
            return false;
        } else if (condition.contains("&")) {
            String[] conditions = condition.split("&");
            for(String newCondition: conditions)
                if (!conditionTriggered(log, newCondition))
                    return false;
            return true;
        } else {
            String[] temp = condition.split("=");
            String key = temp[0];
            String value = temp.length==1?"":temp[1];
            if(key.equals("type"))
                return value.equals(log.getType());
            else if (key.equals("table_name"))
                return value.equals(log.getTable_name());
            else if (key.equals("raw_id"))
                return value.equals(""+log.getRaw_id());
            else {
                key = key.replaceFirst("info\\.", "");
                temp = log.getInfo().split("&");
                for(String info: temp){
                    String[] temp2 = info.split("=");
                    if(key.equals(temp2[0])&&value.equals(temp2.length==1?"":temp2[1]))
                        return true;
                }
                return false;
            }
        }
    }
}
