package com.example.lab4.jms;

import com.example.lab4.model.Log;
import com.example.lab4.model.ConditionEmail;
import com.example.lab4.service.IConditionEmailService;
import com.example.lab4.utils.ConditionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Watchdog {
    private final IConditionEmailService conditionEmailService;
    @Autowired
    public Watchdog(IConditionEmailService conditionEmailService) {
        this.conditionEmailService = conditionEmailService;
    }

    @JmsListener(destination = "log")
    public void receiveMessage(Log log) {
        List<ConditionEmail> conditionEmailList = conditionEmailService.findAll();
        for(ConditionEmail conditionEmail: conditionEmailList){
            if(ConditionChecker.conditionTriggered(log, conditionEmail.getCondition())) {
                System.out.println("condition triggered: " + conditionEmail.getCondition());
                System.out.println("with log: " + log);
                System.out.println("sent to: " + conditionEmail.getEmail());
                System.out.println("-------------");
            }
        }
    }
}
