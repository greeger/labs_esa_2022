package com.example.lab4.jms;

import com.example.lab4.model.Log;
import com.example.lab4.service.ILogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Logger {
    private final ILogService logService;
    @Autowired
    public Logger(ILogService logService) {
        this.logService = logService;
    }

    @JmsListener(destination = "log")
    public void receiveMessage(Log log) {
        logService.add(log);
    }
}
