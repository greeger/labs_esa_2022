package com.example.lab4.jms;

import com.example.lab4.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.jms.core.JmsTemplate;

@Service
public class Sender {
    private final JmsTemplate jmsTemplate;
    @Autowired
    public Sender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendMessage(Log log)
    {
        jmsTemplate.convertAndSend("log", log);
    }
}