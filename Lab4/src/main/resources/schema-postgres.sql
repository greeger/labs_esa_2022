DROP TABLE IF EXISTS Movie;
DROP TABLE IF EXISTS Director;

CREATE TABLE Movie
(
    id           SERIAL PRIMARY KEY,
    name         TEXT,
    year         INT,
    director_id  INT,
    imdb         TEXT
);
CREATE TABLE Director
(
    id           SERIAL PRIMARY KEY,
    name         TEXT,
    imdb         TEXT
);

ALTER TABLE Movie
    ADD CONSTRAINT fk FOREIGN KEY (director_id) REFERENCES director (id) ON DELETE CASCADE;

DROP TABLE IF EXISTS Log;
CREATE TABLE Log
(
    id           SERIAL PRIMARY KEY,
    type         TEXT,
    table_name   TEXT,
    raw_id       INT,
    info         TEXT
);

DROP TABLE IF EXISTS Condition_Email;
CREATE TABLE Condition_Email
(
    id           SERIAL PRIMARY KEY,
    condition    TEXT,
    email        TEXT
);