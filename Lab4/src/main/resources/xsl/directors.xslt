<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">
    <xsl:output method="html" indent="yes" media-type="text/html" encoding="UTF-8" />
    <xsl:template match="/">
        <html>
            <head>
                <title>All directors</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            </head>
            <body>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>IMDB</th>
                    </tr>

                    <xsl:for-each select="ArrayList/item">
                        <xsl:variable name="id" select="id"/>
                        <xsl:variable name="imdb" select="imdb"/>
                        <tr>
                            <td><a href="/movies/{$id}"><xsl:value-of select="name"/></a></td>
                            <td><a href="https://www.imdb.com/name/{$imdb}"><xsl:value-of select="imdb"/></a></td>

                            <td>
                                <form action="/directors/{$id}" method="post">
                                    <input type="submit" value="delete" />
                                </form>
                            </td>
                        </tr>
                    </xsl:for-each>
                    <tr>
                        <form action="/directors" method="post">
                            <td><input type="text" name="name" /></td>
                            <td><input type="text" name="imdb" /></td>
                            <td><input type="submit" value="add" /></td>
                        </form>
                    </tr>

                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>