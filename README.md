# Labs for enterprise systems architecture

## Lab1

Technologies: EJB, glassfish, postgreSQL, hibernate, javax-servlet

UI:

![lab1_directors](/pics/lab1_directors.png)
![lab1_movies](/pics/lab1_movies.png)


## Lab2

Technologies: spring_framework, postgreSQL, hibernate

UI:

![lab2_directors](/pics/lab2_directors.png)
![lab2_movies](/pics/lab2_movies.png)


## Lab3

Technologies: REST, spring_framework, postgreSQL, hibernate, xsl

UI:

![lab3_directors](/pics/lab3_directors.png)
![lab3_movies](/pics/lab3_movies.png)


## Lab4

Technologies: REST, spring_framework, postgreSQL, hibernate, xsl, JMS

Logs:

![lab4_logs](/pics/lab4_logs.png)

Triggers:

![lab4_triggers](/pics/lab4_triggers.png)