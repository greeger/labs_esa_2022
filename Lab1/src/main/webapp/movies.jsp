<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>${name}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <a href="directors">All directors</a><br>
    Directed by ${name}
    <table>
        <tr>
            <th>Name</th>
            <th>Year</th>
            <th>IMDB</th>
        </tr>

        <c:forEach var="movie" items="${movies}">
            <tr>
                <td>${movie.name}</td>
                <td>${movie.year}</td>
                <td><a href="https://www.imdb.com/title/${movie.imdb}">${movie.imdb}</a></td>
                <td>
                    <form action="movies?action=delete" method="post">
                        <input type="hidden" name="directorId" value="${id}" />
                        <input type="hidden" name="directorName" value="${name}" />
                        <input type="hidden" name="id" value="${movie.id}" />
                        <input type="submit" value="delete" />
                    </form>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <form action="movies?action=add" method="post">
                <input type="hidden" name="directorId" value="${id}" />
                <input type="hidden" name="directorName" value="${name}" />
                <td><input type="text" name="name" /></td>
                <td><input type="number" name="year" /></td>
                <td><input type="text" name="imdb" /></td>
                <td><input type="submit" value="add" /></td>
            </form>
        </tr>

    </table>
</body>
</html>