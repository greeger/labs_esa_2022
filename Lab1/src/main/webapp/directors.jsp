<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>All directors</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <table>
        <tr>
            <th>Name</th>
            <th>IMDB</th>
        </tr>

        <c:forEach var="director" items="${directors}">
            <tr>
                <td><a href="movies?id=${director.id}&name=${director.name}">${director.name}</a></td>
                <td><a href="https://www.imdb.com/name/${director.imdb}">${director.imdb}</a></td>
                <td>
                    <form action="directors?action=delete" method="post">
                        <input type="hidden" name="id" value="${director.id}" />
                        <input type="submit" value="delete" />
                    </form>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <form action="directors?action=add" method="post">
                <td><input type="text" name="name" /></td>
                <td><input type="text" name="imdb" /></td>
                <td><input type="submit" value="add" /></td>
            </form>
        </tr>

    </table>
</body>
</html>