package com.example.repository;

import com.example.domain.Director;

import java.util.List;

import jakarta.ejb.Stateless;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateless
public class DirectorRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public Director add(Director director){
        return entityManager.merge(director);
    }

    public Director get(long id){
        return entityManager.find(Director.class, id);
    }

    public void update(Director director){
        entityManager.merge(director);
    }

    public void delete(long id){
        entityManager.remove(get(id));
    }

    public List<Director> getAll(){
        return entityManager.createQuery("SELECT d FROM director d").getResultList();
    }
}
