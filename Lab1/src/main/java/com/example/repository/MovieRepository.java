package com.example.repository;

import com.example.domain.Movie;

import java.util.List;

import jakarta.ejb.Stateless;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateless
public class MovieRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public Movie add(Movie movie){
        return entityManager.merge(movie);
    }

    public Movie get(long id){
        return entityManager.find(Movie.class, id);
    }

    public void update(Movie movie){
        add(movie);
    }

    public void delete(long id){
        entityManager.remove(get(id));
    }

    public List<Movie> getAll(){
        return entityManager.createQuery("SELECT m FROM movie m").getResultList();
    }

    public List<Movie> getByDirectorId(long id){
        return entityManager.createQuery("SELECT m FROM movie m where director="+id).getResultList();
    }
}
