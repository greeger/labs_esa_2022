package com.example.servlets;

import com.example.domain.Director;
import com.example.domain.Movie;
import com.example.repository.DirectorRepository;
import com.example.repository.MovieRepository;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "moviesServlet", value = "/movies")
public class MoviesServlet extends HttpServlet {

    @EJB
    private MovieRepository movieRepository;
    @EJB
    private DirectorRepository directorRepository;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        List<Movie> movies =  movieRepository.getByDirectorId(id);
        request.setAttribute("movies", movies);
        request.setAttribute("id", id);
        request.setAttribute("name", name);

        getServletContext().getRequestDispatcher("/movies.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int directorId = Integer.parseInt(request.getParameter("directorId"));
        String directorName = request.getParameter("directorName");
        String action = request.getParameter("action");
        switch (action) {
            case "add":
                Director director = directorRepository.get(directorId);
                String name = request.getParameter("name");
                int year = Integer.parseInt(request.getParameter("year"));
                String imdb = request.getParameter("imdb");
                Movie movie = new Movie(name, year, director, imdb);
                movieRepository.add(movie);
                break;
            case "delete":
                int id = Integer.parseInt(request.getParameter("id"));
                movieRepository.delete(id);
                break;
            default:
                break;
        }

        List<Movie> movies =  movieRepository.getByDirectorId(directorId);
        request.setAttribute("movies", movies);
        request.setAttribute("id", directorId);
        request.setAttribute("name", directorName);

        getServletContext().getRequestDispatcher("/movies.jsp").forward(request, response);
    }
}