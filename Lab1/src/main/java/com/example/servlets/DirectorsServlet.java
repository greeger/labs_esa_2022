package com.example.servlets;

import com.example.domain.Director;
import com.example.repository.DirectorRepository;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "directorsServlet", value = "/directors")
public class DirectorsServlet extends HttpServlet {

    @EJB
    private DirectorRepository directorRepository;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Director> directors =  directorRepository.getAll();
        request.setAttribute("directors", directors);

        getServletContext().getRequestDispatcher("/directors.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");
        switch (action) {
            case "add":
                String name = request.getParameter("name");
                String imdb = request.getParameter("imdb");
                Director director = new Director(name, imdb);
                directorRepository.add(director);
                break;
            case "delete":
                int id = Integer.parseInt(request.getParameter("id"));
                directorRepository.delete(id);
                break;
            default:
                break;
        }

        List<Director> directors =  directorRepository.getAll();
        request.setAttribute("directors", directors);

        getServletContext().getRequestDispatcher("/directors.jsp").forward(request, response);
    }
}